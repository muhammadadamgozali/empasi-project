package com.material.empasianak

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashScreenActivity:AppCompatActivity() {
    private val coroutineScope = CoroutineScope(Dispatchers.Main)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)

        coroutineScope.launch {
            delay(2000)
            if (!isLoggedIn()) {
                val intent = Intent(this@SplashScreenActivity, MenuAwalActivity::class.java)
                startActivity(intent)
                finish() // Menyelesaikan aktivitas ini sehingga pengguna tidak dapat kembali ke sini tanpa login
            }else{
                val intent = Intent(this@SplashScreenActivity, MenuUtamaActivity::class.java)
                startActivity(intent)
                finish() // Menyelesaikan aktivitas ini sehingga pengguna tidak dapat kembali ke sini tanpa login
            }
        }

    }
    private fun isLoggedIn(): Boolean {
        val sharedPreferences = getSharedPreferences("login_pref", Context.MODE_PRIVATE)
        return sharedPreferences.getBoolean("isLoggedIn", false)
    }
}