package com.material.empasianak

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class PilihGrafikActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pilih_graphic_bar_layout)

        val btn_laki_0_24 : Button = findViewById(R.id.btn_laki_0_24)
        val btn_laki_24_60 : Button = findViewById(R.id.btn_laki_24_60)
        val btn_perempuan_0_24 : Button = findViewById(R.id.btn_perempuan_0_24)
        val btn_perempuan_24_60 : Button = findViewById(R.id.btn_perempuan_24_60)

        btn_laki_0_24.setOnClickListener {
            val intent = Intent(this, GraphicBarActivity::class.java)
            intent.putExtra("pilihan", "laki_0_24")
            startActivity(intent)
        }

        btn_laki_24_60.setOnClickListener {
            val intent = Intent(this, GraphicBarActivity::class.java)
            intent.putExtra("pilihan", "laki_24_60")
            startActivity(intent)
        }

        btn_perempuan_0_24.setOnClickListener {
            val intent = Intent(this, GraphicBarActivity::class.java)
            intent.putExtra("pilihan", "perempuan_0_24")
            startActivity(intent)
        }

        btn_perempuan_24_60.setOnClickListener {
            val intent = Intent(this, GraphicBarActivity::class.java)
            intent.putExtra("pilihan", "perempuan_24_60")
            startActivity(intent)
        }
    }
}