package com.material.empasianak

import android.os.Parcel
import com.google.android.material.datepicker.CalendarConstraints
import java.util.Calendar

class CustomDateValidator : CalendarConstraints.DateValidator {
    override fun isValid(date: Long): Boolean {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = date

        // Validasi jika tanggal berada di minggu kedua dan hari Rabu
        return calendar.get(Calendar.WEEK_OF_MONTH) == 2 && calendar.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeInt(0)
    }

    override fun describeContents(): Int {
        return 0
    }
}