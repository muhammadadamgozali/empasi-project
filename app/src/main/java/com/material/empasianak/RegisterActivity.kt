package com.material.empasianak

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText

class RegisterActivity : AppCompatActivity() {
    private lateinit var dbHelper: DatabaseHelper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.register_layout)

        dbHelper = DatabaseHelper(this)

        val namalengkap: TextInputEditText = findViewById(R.id.it_namalengkap)
        val nomortelepon: TextInputEditText = findViewById(R.id.it_nomortelepon)
        val alamat: TextInputEditText = findViewById(R.id.it_alamat)
        val usernameSet: TextInputEditText = findViewById(R.id.it_username)
        val password: TextInputEditText = findViewById(R.id.it_password)
        val repassword: TextInputEditText = findViewById(R.id.it_repassword)

        val btn_proses_register: Button = findViewById(R.id.btn_proses_simpan)
        btn_proses_register.setOnClickListener {

            val nama_lengkap = namalengkap.text.toString()
            val nomor_telepon = nomortelepon.text.toString()
            val alamat_lengkap = alamat.text.toString()
            val username = usernameSet.text.toString()
            val pass = password.text.toString()
            val repasswordSet = repassword.text.toString()


            if (username.isEmpty() || pass.isEmpty() || nama_lengkap.isEmpty() || alamat_lengkap.isEmpty() || nomor_telepon.isEmpty() || repasswordSet.isEmpty()) {
                Toast.makeText(this, "Data harus diisi semua", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            //  Log.d("infoempasi", "Proses "+username+ " "+password)

            val db = dbHelper.readableDatabase

            //Check data username yg sama.
            val selection = "username = ?"
            val selectionArgs = arrayOf(username)
            val cursor = db.query("table_login", null, selection, selectionArgs, null, null, null)
            if (cursor.moveToFirst()) { // Data Ada, Isi yg lain lagi
                Toast.makeText(this, "Username Sudah Ada", Toast.LENGTH_LONG).show()
            } else {

                if (pass != repasswordSet) {
                    Toast.makeText(this, "Password tidak cocok", Toast.LENGTH_LONG).show()
                } else {
                    val db = dbHelper.writableDatabase
                    val values = ContentValues().apply {
                        put("username", username)
                        put("pass", pass)
                        put("nama_lengkap", nama_lengkap)
                        put("alamat_lengkap", alamat_lengkap)
                        put("no_telepon", nomor_telepon)
                    }

                    val insertedRowId = db.insert("table_login", null, values)
                    if (insertedRowId != -1L) {
                        Toast.makeText(this, "Data berhasil ditambahkan!", Toast.LENGTH_LONG).show()
                        // Login berhasil, pindah ke layout dashboard
                        val sharedPreferences = getSharedPreferences("login_pref", Context.MODE_PRIVATE)
                        val editor = sharedPreferences.edit()
                        editor.putBoolean("isLoggedIn", true)
                        editor.putString("namaLengkap",nama_lengkap)
                        editor.apply()

                        val intent = Intent(this, MenuUtamaActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else {
                        Toast.makeText(this, "Gagal menambahkan data.", Toast.LENGTH_LONG).show()
                    }

                }

            }

            cursor.close()
            db.close()

        }
    }

    override fun onResume() {
        super.onResume()

        if (isLoggedIn()) {
            val intent = Intent(this, MenuUtamaActivity::class.java)
            startActivity(intent)
            finish() // Menyelesaikan aktivitas ini
        }
    }
    private fun isLoggedIn(): Boolean {
        val sharedPreferences = getSharedPreferences("login_pref", Context.MODE_PRIVATE)
        return sharedPreferences.getBoolean("isLoggedIn", false)
    }
}