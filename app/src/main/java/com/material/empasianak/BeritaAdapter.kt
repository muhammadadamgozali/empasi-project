package com.material.empasianak

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class BeritaAdapter(private val beritaList: List<Berita>) : RecyclerView.Adapter<BeritaAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_berita_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val berita = beritaList[position]

        val formattedDate = DateUtils.formatDate(berita.txt_date_post)

        holder.txt_judul_berita.text = berita.txt_judul_berita
        holder.txt_deskripsi.text = berita.txt_deskripsi
        //holder.txt_isi_berita.text = berita.txt_isi_berita
        holder.txt_date_post.text = formattedDate
        holder.txt_views.text = berita.txt_views
    }

    override fun getItemCount(): Int {
        return beritaList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txt_judul_berita: TextView = itemView.findViewById(R.id.txt_judul_berita)
        val txt_deskripsi: TextView = itemView.findViewById(R.id.txt_deskripsi)
        //val txt_isi_berita: TextView = itemView.findViewById(R.id.txt_isi_berita)
        val txt_date_post: TextView = itemView.findViewById(R.id.txt_date_post)
        val txt_views: TextView = itemView.findViewById(R.id.txt_views)
    }
}