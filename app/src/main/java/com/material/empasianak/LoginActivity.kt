package com.material.empasianak

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputEditText

class LoginActivity:AppCompatActivity() {
    private lateinit var dbHelper: DatabaseHelper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_layout)

        dbHelper = DatabaseHelper(this)

        /*val btn_back_to_awal: ImageView = findViewById(R.id.btn_back_to_awal)
        btn_back_to_awal.setOnClickListener {
            val intent = Intent(this, MenuAwalActivity::class.java)
            startActivity(intent)
            finish()
        }*/

        val username: TextInputEditText = findViewById(R.id.it_username)
        val password: TextInputEditText = findViewById(R.id.it_password)

        val btn_proses_login: Button = findViewById(R.id.btn_proses_simpan)
        btn_proses_login.setOnClickListener {
            val usernameText = username.text.toString()
            val passwordText = password.text.toString()

            /*Toast.makeText(this, usernameText+ ' '+passwordText, Toast.LENGTH_SHORT).show()*/

            if (usernameText.isEmpty() || passwordText.isEmpty()) {
                Toast.makeText(this, "Username dan Password harus diisi", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            //  Log.d("infoempasi", "Proses "+username+ " "+password)

            val db = dbHelper.readableDatabase

            val selection = "username = ? AND pass = ?"
            val selectionArgs = arrayOf(usernameText, passwordText)
            val cursor = db.query("table_login", null, selection, selectionArgs, null, null, null)

            var idUser = ""
            var namaLengkap = ""
            if (cursor != null && cursor.moveToFirst()) {
                val columnID = cursor.getColumnIndex("id_user")
                val columnIndex = cursor.getColumnIndex("nama_lengkap")
                if (columnIndex >= 0) {
                    idUser = cursor.getString(columnID)
                    namaLengkap = cursor.getString(columnIndex)
                }

                // Login berhasil, pindah ke layout dashboard
                val sharedPreferences = getSharedPreferences("login_pref", Context.MODE_PRIVATE)
                val editor = sharedPreferences.edit()
                editor.putBoolean("isLoggedIn", true)
                editor.putString("idUser",idUser)
                editor.putString("namaLengkap",namaLengkap)
                editor.apply()


                Toast.makeText(this, "Login Berhasil", Toast.LENGTH_SHORT).show()
                val intent = Intent(this, MenuUtamaActivity::class.java)
                startActivity(intent)
                finish() // Menyelesaikan aktivitas ini sehingga pengguna tidak dapat kembali ke sini tanpa logout

            } else {
                Toast.makeText(this, "Login gagal", Toast.LENGTH_SHORT).show()
            }

            cursor.close()
            db.close()


        }
    }

    override fun onResume() {
        super.onResume()

        if (isLoggedIn()) {
            val intent = Intent(this, MenuUtamaActivity::class.java)
            startActivity(intent)
            finish() // Menyelesaikan aktivitas ini
        }
    }
    private fun isLoggedIn(): Boolean {
        val sharedPreferences = getSharedPreferences("login_pref", Context.MODE_PRIVATE)
        return sharedPreferences.getBoolean("isLoggedIn", false)
    }
}