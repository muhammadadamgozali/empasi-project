package com.material.empasianak

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet

class GraphicBarActivity : AppCompatActivity() {
    private lateinit var dbHelper: DatabaseHelper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.graphic_bar_layout)

        val lineChart = findViewById<LineChart>(R.id.lineChart)

        dbHelper = DatabaseHelper(this)
        val db = dbHelper.readableDatabase

        // Ambil data
        val pilihan = intent.getStringExtra("pilihan")
        var table_set = ""
        var deskripsi_grafik = ""
        if(pilihan == "laki_0_24"){
            table_set = "table_0_24_laki"
            deskripsi_grafik = "Berat Badan Menurut Tinggi Badan (Laki-Laki 0-24 Bulan)"
        }else if(pilihan == "laki_24_60"){
            table_set = "table_24_60_laki"
            deskripsi_grafik = "Berat Badan Menurut Tinggi Badan (Laki-Laki 24-60 Bulan)"
        }else if(pilihan == "perempuan_0_24"){
            table_set = "table_0_24_perempuan"
            deskripsi_grafik = "Berat Badan Menurut Tinggi Badan (Perempuan 0-24 Bulan)"
        }else if(pilihan == "perempuan_24_60"){
            table_set = "table_24_60_perempuan"
            deskripsi_grafik = "Berat Badan Menurut Tinggi Badan (Perempuan 24-60 Bulan)"
        }


        val entries_0_24_laki_min_3 = ArrayList<Entry>()
        val entries_0_24_laki_min_2 = ArrayList<Entry>()
        val entries_0_24_laki_min_1 = ArrayList<Entry>()
        val entries_0_24_laki_median = ArrayList<Entry>()
        val entries_0_24_laki_plus_1 = ArrayList<Entry>()
        val entries_0_24_laki_plus_2 = ArrayList<Entry>()
        val entries_0_24_laki_plus_3 = ArrayList<Entry>()
        val cursor = db.rawQuery("SELECT * FROM ${table_set}", null)

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    val panjang_badan = cursor.getFloat(cursor.getColumnIndex("panjang_badan"))
                    val min_3 = cursor.getFloat(cursor.getColumnIndex("min_3"))
                    val min_2 = cursor.getFloat(cursor.getColumnIndex("min_2"))
                    val min_1 = cursor.getFloat(cursor.getColumnIndex("min_1"))
                    val median = cursor.getFloat(cursor.getColumnIndex("median"))
                    val plus_1 = cursor.getFloat(cursor.getColumnIndex("plus_1"))
                    val plus_2 = cursor.getFloat(cursor.getColumnIndex("plus_2"))
                    val plus_3 = cursor.getFloat(cursor.getColumnIndex("plus_3"))

                    val entry1 = Entry(panjang_badan, min_3)
                    val entry2 = Entry(panjang_badan, min_2)
                    val entry3 = Entry(panjang_badan, min_1)
                    val entry4 = Entry(panjang_badan, median)
                    val entry5 = Entry(panjang_badan, plus_1)
                    val entry6 = Entry(panjang_badan, plus_2)
                    val entry7 = Entry(panjang_badan, plus_3)

                    entries_0_24_laki_min_3.add(entry1)
                    entries_0_24_laki_min_2.add(entry2)
                    entries_0_24_laki_min_1.add(entry3)
                    entries_0_24_laki_median.add(entry4)
                    entries_0_24_laki_plus_1.add(entry5)
                    entries_0_24_laki_plus_2.add(entry6)
                    entries_0_24_laki_plus_3.add(entry7)
                } while (cursor.moveToNext())
            }
            cursor.close()
        }

        val dataSet1 = LineDataSet(entries_0_24_laki_min_3, "-3")
        dataSet1.color = ContextCompat.getColor(this, R.color.darkred)
        dataSet1.valueTextColor = ContextCompat.getColor(this, R.color.darkred)
        dataSet1.setCircleColor(ContextCompat.getColor(this, R.color.darkred))

        val dataSet2 = LineDataSet(entries_0_24_laki_min_2, "-3")
        dataSet2.color = ContextCompat.getColor(this, R.color.red)
        dataSet2.valueTextColor = ContextCompat.getColor(this, R.color.red)
        dataSet2.setCircleColor(ContextCompat.getColor(this, R.color.red))

        val dataSet3 = LineDataSet(entries_0_24_laki_min_1, "-1")
        dataSet3.color = ContextCompat.getColor(this, R.color.kuning)
        dataSet3.valueTextColor = ContextCompat.getColor(this, R.color.kuning)
        dataSet3.setCircleColor(ContextCompat.getColor(this, R.color.kuning))

        val dataSet4 = LineDataSet(entries_0_24_laki_median, "Median")
        dataSet4.color = ContextCompat.getColor(this, R.color.hijau)
        dataSet4.valueTextColor = ContextCompat.getColor(this, R.color.hijau)
        dataSet4.setCircleColor(ContextCompat.getColor(this, R.color.hijau))

        val dataSet5 = LineDataSet(entries_0_24_laki_plus_1, "+1")
        dataSet5.color = ContextCompat.getColor(this, R.color.kuning)
        dataSet5.valueTextColor = ContextCompat.getColor(this, R.color.kuning)
        dataSet5.setCircleColor(ContextCompat.getColor(this, R.color.kuning))

        val dataSet6 = LineDataSet(entries_0_24_laki_plus_2, "+2")
        dataSet6.color = ContextCompat.getColor(this, R.color.red)
        dataSet6.valueTextColor = ContextCompat.getColor(this, R.color.red)
        dataSet6.setCircleColor(ContextCompat.getColor(this, R.color.red))

        val dataSet7 = LineDataSet(entries_0_24_laki_plus_3, "+3")
        dataSet7.color = ContextCompat.getColor(this, R.color.darkred)
        dataSet7.valueTextColor = ContextCompat.getColor(this, R.color.darkred)
        dataSet7.setCircleColor(ContextCompat.getColor(this, R.color.darkred))

// Gabungkan semua dataset ke dalam LineData
        val lineData = LineData(dataSet1, dataSet2, dataSet3, dataSet4, dataSet5, dataSet6, dataSet7)

        lineChart.data = lineData

        lineChart.setTouchEnabled(true)
        lineChart.setPinchZoom(true)

        lineChart.description.text = deskripsi_grafik
    }
}