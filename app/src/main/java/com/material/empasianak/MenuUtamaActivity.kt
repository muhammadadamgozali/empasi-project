package com.material.empasianak

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import java.text.SimpleDateFormat
import java.util.Locale

class MenuUtamaActivity:AppCompatActivity() {
    private lateinit var txtNamaLengkap: TextView
    private lateinit var recyclerView: RecyclerView
    private lateinit var beritaAdapter: BeritaAdapter
    private val dataBerita = mutableListOf<Berita>()
    private lateinit var databaseHelper: DatabaseHelper

    private lateinit var dateFormatter: SimpleDateFormat
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.menu_utama)

        txtNamaLengkap = findViewById(R.id.txtNamaLengkap)
        val sharedPreferences: SharedPreferences = getSharedPreferences("login_pref", Context.MODE_PRIVATE)
        val namaLengkap = sharedPreferences.getString("namaLengkap", "")

        if (!namaLengkap.isNullOrEmpty()) {
            txtNamaLengkap.text = namaLengkap.uppercase()
        }

        //btn_diagnosa_anak di tekan-------------------------------------
        val btn_diagnosa_anak: Button = findViewById(R.id.btn_diagnosa_anak)
        btn_diagnosa_anak.setOnClickListener {
            val intent = Intent(this, DiagnosaAnakActivity::class.java)
            startActivity(intent)
            /*finish()*/
        }

        val btn_grafik_pilih: Button = findViewById(R.id.btn_laki_24_60)
        btn_grafik_pilih.setOnClickListener {
            val intent = Intent(this, PilihGrafikActivity::class.java)
            startActivity(intent)
            /*finish()*/
        }

        val btn_materi_edukasi: Button = findViewById(R.id.btn_materi_edukasi)
        btn_materi_edukasi.setOnClickListener {
            val intent = Intent(this, MateriEdukasiActivity::class.java)
            startActivity(intent)
            /*finish()*/
        }

        val btn_jadwal_posyandu: Button = findViewById(R.id.btn_jadwal_posyandu)
        val customValidator = CustomDateValidator()
        btn_jadwal_posyandu.setOnClickListener {
            val datePicker: MaterialDatePicker<Long> = MaterialDatePicker
                .Builder
                .datePicker()
                .setInputMode(MaterialDatePicker.INPUT_MODE_CALENDAR )
                .setTitleText("Jadwal Posyandu")
                .setCalendarConstraints(CalendarConstraints.Builder().setValidator(customValidator).build())
                .build()
            datePicker.show( supportFragmentManager , "DATE_PICKER")

            datePicker.addOnPositiveButtonClickListener {
                val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
                val date = sdf.format( it )
            }
        }

        //RECYLE_BERITA----------------------
        recyclerView = findViewById(R.id.RV_Berita)
        recyclerView.layoutManager = LinearLayoutManager(this)
        beritaAdapter = BeritaAdapter(dataBerita)
        recyclerView.adapter = beritaAdapter

        databaseHelper = DatabaseHelper(this)

        val db: SQLiteDatabase = databaseHelper.readableDatabase
        val cursor: Cursor = db.query(
            "table_berita",
            null, null, null, null, null, null
        )

        while (cursor.moveToNext()) {
            val id = cursor.getLong(cursor.getColumnIndex("id_berita"))
            val judul_berita = cursor.getString(cursor.getColumnIndex("judul_berita"))
            val deskripsi = cursor.getString(cursor.getColumnIndex("deskripsi"))
            val date_post = cursor.getString(cursor.getColumnIndex("tanggal_post"))
            val total_view = cursor.getString(cursor.getColumnIndex("total_view"))

            val beritaItem = Berita(id, judul_berita, deskripsi, date_post,total_view)
            dataBerita.add(beritaItem)
        }

        cursor.close()
        db.close()

        beritaAdapter.notifyDataSetChanged()



    }
}