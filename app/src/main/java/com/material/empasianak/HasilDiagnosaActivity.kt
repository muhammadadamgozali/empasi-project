package com.material.empasianak

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout

class HasilDiagnosaActivity : AppCompatActivity() {
    private lateinit var dbHelper: DatabaseHelper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.hasil_diagnosa_anak_layout)

        dbHelper = DatabaseHelper(this)

        // Ambil data
        val usia_anak = intent.getStringExtra("usia_anak")
        val usia_anak_integer = usia_anak?.toFloat()
        val jenis_kelamin = intent.getStringExtra("jenis_kelamin")
        val panjang_badan = intent.getStringExtra("panjang_badan")
        val berat_badan = intent.getStringExtra("berat_badan")

        val txt_usia_anak : TextView = findViewById(R.id.txt_usia_anak)
        val txt_jenis_kelamin : TextView = findViewById(R.id.txt_jenis_kelamin)
        val txt_panjang_badan : TextView = findViewById(R.id.txt_panjang_badan)
        val txt_berat_badan : TextView = findViewById(R.id.txt_berat_badan)
        val txt_hasil_status : TextView = findViewById(R.id.txt_hasil_status)
        val txt_keterangan_status : TextView = findViewById(R.id.txt_keterangan_status)
        val CL_HASIL_SET : ConstraintLayout = findViewById(R.id.CL_HASIL_SET)

        txt_usia_anak.text = usia_anak+" Bulan"
        txt_jenis_kelamin.text = jenis_kelamin
        txt_panjang_badan.text = panjang_badan+" Cm"
        txt_berat_badan.text = berat_badan +" Kg"

        var table_pilihan: String = ""
        //Analisa Data
        if(usia_anak != "" && usia_anak_integer != null && jenis_kelamin != "" && panjang_badan != "" && berat_badan !="") {
            if (usia_anak_integer >= 0 && usia_anak_integer <= 24) {
                if (jenis_kelamin == "Anak Laki-Laki") {
                    table_pilihan = "table_0_24_laki"
                } else {
                    table_pilihan = "table_0_24_perempuan"
                }
            } else if (usia_anak_integer > 24 && usia_anak_integer <= 60) {
                if (jenis_kelamin == "Anak Laki-Laki") {
                    table_pilihan = "table_24_60_laki"
                } else {
                    table_pilihan = "table_24_60_perempuan"
                }
            }

            //Ambil data dari database berdasarkan panjang badan
            val db = dbHelper.readableDatabase
            val query = "SELECT * FROM ${table_pilihan} where CAST(panjang_badan AS REAL) <= ? ORDER BY CAST(panjang_badan AS REAL) DESC LIMIT 1"
            val cursor = db.rawQuery(query, arrayOf(panjang_badan.toString()))

            val beratBadanList = mutableListOf<String>()

            if (cursor.moveToFirst()) {
                // Ambil data dari cursor
                val panjangBadan = cursor.getDouble(cursor.getColumnIndex("panjang_badan"))
                val min_3 = cursor.getString(cursor.getColumnIndex("min_3"))
                val min_2 = cursor.getString(cursor.getColumnIndex("min_2"))
                val min_1 = cursor.getString(cursor.getColumnIndex("min_1"))
                val median = cursor.getString(cursor.getColumnIndex("median"))
                val plus_1 = cursor.getString(cursor.getColumnIndex("plus_1"))
                val plus_2 = cursor.getString(cursor.getColumnIndex("plus_2"))
                val plus_3 = cursor.getString(cursor.getColumnIndex("plus_3"))

                //Toast.makeText(this@HasilDiagnosaActivity, "Input : "+min_3+" Hasil :"+panjangBadan.toString(), Toast.LENGTH_SHORT).show()

                var kategori:Int = 0
                var kategoriText:String = ""
                var kategoriStatus:String = ""
                if (berat_badan != null) {
                    if(berat_badan.toFloat() >= 0.0 && berat_badan.toFloat() < min_3.toFloat()){ // 0 - 2.1
                        kategori = 1
                        txt_hasil_status.text = "< -3 SD"
                        txt_keterangan_status.text = "Gizi buruk\n(Severely wasted)"
                        CL_HASIL_SET.setBackgroundResource(R.drawable.rounded_red_dark)
                    }else if(berat_badan.toFloat() >= min_3.toFloat() && berat_badan.toFloat() < min_2.toFloat()) { // 2.2 - 2.3
                        kategori = 2
                        txt_hasil_status.text = "-3 SD sd <- 2 SD"
                        txt_keterangan_status.text = "Gizi Kurang\n(Wasted)"
                        CL_HASIL_SET.setBackgroundResource(R.drawable.rounded_red)
                    }else if(berat_badan.toFloat() >= min_2.toFloat() && berat_badan.toFloat() < min_1.toFloat()) { // 2.4 - 2.5
                        kategori = 3
                        txt_hasil_status.text = "-2 SD sd +1 SD"
                        txt_keterangan_status.text = "Gizi Baik\n(Normal)"
                        CL_HASIL_SET.setBackgroundResource(R.drawable.rounded_green)
                    }else if(berat_badan.toFloat() >= min_1.toFloat() && berat_badan.toFloat() < median.toFloat()) { // 2.6 - 2.8
                        kategori = 3
                        txt_hasil_status.text = "-2 SD sd +1 SD"
                        txt_keterangan_status.text = "Gizi Baik\n(Normal)"
                        CL_HASIL_SET.setBackgroundResource(R.drawable.rounded_green)
                    }else if(berat_badan.toFloat() == median.toFloat()){ // 2.9
                        kategori = 3
                        txt_hasil_status.text = "-2 SD sd +1 SD"
                        txt_keterangan_status.text = "Gizi Baik\n(Normal)"
                        CL_HASIL_SET.setBackgroundResource(R.drawable.rounded_green)
                    }else if(berat_badan.toFloat() <= plus_1.toFloat() && berat_badan.toFloat() > median.toFloat()){ // 2.8 - 3.2
                        kategori = 3
                        txt_hasil_status.text = "-2 SD sd +1 SD"
                        txt_keterangan_status.text = "Gizi Baik\n(Normal)"
                        CL_HASIL_SET.setBackgroundResource(R.drawable.rounded_green)
                    }else if(berat_badan.toFloat() <= plus_2.toFloat() && berat_badan.toFloat() > plus_1.toFloat()){ // 3.3 - 3.5
                        kategori = 4
                        txt_hasil_status.text = "> +1 SD sd +2 SD"
                        txt_keterangan_status.text = "Berisiko gizi lebih\n(Possible risk of overweight)"
                        CL_HASIL_SET.setBackgroundResource(R.drawable.rounded_red)
                    }else if(berat_badan.toFloat() <= plus_3.toFloat() && berat_badan.toFloat() > plus_2.toFloat()){ // 3.6 - 3.8
                        kategori = 5
                        txt_hasil_status.text = "> +2 SD sd +3 SD"
                        txt_keterangan_status.text = "Gizi lebih\n(Overweight)"
                        CL_HASIL_SET.setBackgroundResource(R.drawable.rounded_red_dark)
                    }else if(berat_badan.toFloat() > plus_3.toFloat()) { // 3.9 ++
                        kategori = 6
                        txt_hasil_status.text = "> +3 SD"
                        txt_keterangan_status.text = "Obesitas\n(Obese)"
                        CL_HASIL_SET.setBackgroundResource(R.drawable.rounded_red_dark)
                    }else{
                        kategori = 0
                        txt_hasil_status.text = "Unknown"
                        txt_keterangan_status.text = "Unknown"
                        CL_HASIL_SET.setBackgroundResource(R.drawable.rounded_black)
                    }

                }

               // Toast.makeText(this@HasilDiagnosaActivity, kategori.toString()+" "+kategoriText, Toast.LENGTH_SHORT).show()

            }
            cursor.close()


            val btn_saran_empasi: Button = findViewById(R.id.btn_saran_empasi)
            btn_saran_empasi.setOnClickListener {
                val intent = Intent(this, SaranEmpasiActivity::class.java)
                startActivity(intent)
                /*finish()*/
            }

        }
    }

    override fun onResume() {
        super.onResume()

        if (!isLoggedIn()) {
            val intent = Intent(this, MenuUtamaActivity::class.java)
            startActivity(intent)
            finish() // Menyelesaikan aktivitas ini
        }
    }
    private fun isLoggedIn(): Boolean {
        val sharedPreferences = getSharedPreferences("login_pref", Context.MODE_PRIVATE)
        return sharedPreferences.getBoolean("isLoggedIn", false)
    }

    private fun getStatusPilihan(index:Int):MutableList<String>{
        dbHelper = DatabaseHelper(this)

        var indexSet:Int = 0

        if(index == 0) {//Kategori = 1
            indexSet = 1
        }else if(index == 1){//Kategori = 2
            indexSet = 2
        }else if(index == 2){//Kategori = 3
            indexSet = 3
        }else if(index == 3) {//Kategori = 3
            indexSet = 3
        }else if(index == 4) {//Kategori = 3
            indexSet = 3
        }else if(index == 5) {//Kategori = 4
            indexSet = 4
        }else if(index == 6) {//Kategori = 5
            indexSet = 5
        }

        val statusSet = mutableListOf<String>()

        val db = dbHelper.readableDatabase
        val query = "SELECT * FROM table_status_hasil where id = ?"
        val cursor = db.rawQuery(query, arrayOf(indexSet.toString()))

        if (cursor.moveToFirst()) {
            val status = cursor.getString(cursor.getColumnIndex("status"))
            val kategori = cursor.getString(cursor.getColumnIndex("kategori"))

            statusSet.add(status)
            statusSet.add(kategori)
        }

        return statusSet
    }
}