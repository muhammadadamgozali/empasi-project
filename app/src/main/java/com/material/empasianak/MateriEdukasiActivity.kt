package com.material.empasianak

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient

class MateriEdukasiActivity : AppCompatActivity() {
    private lateinit var webView: WebView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.materi_edukasi_layout)

        // Inisialisasi WebView
        webView = findViewById(R.id.webview_information)

        // Pengaturan WebView
        webView.settings.javaScriptEnabled = true
        webView.webChromeClient = WebChromeClient()
        webView.webViewClient = WebViewClient()

        // Muat konten HTML ke WebView
        val htmlContent = """
                            <!DOCTYPE html>
                            <html>
                            <head>
                                <title>PENGENALAN MP-ASI</title>
                                <style>
                                    p{text-align: justify;
                                        text-justify: inter-word;}
                                        h1{font-size:20px;}
                                        h2{font-size:18px;}
                                </style>
                            </head>
                            <body style="padding:10px;font-size:13px">
                                <h1 style="font-size:18px">PENGENALAN MP-ASI</h1>
                        
                                <h2>A. Pengertian</h2>
                                <p>MP-ASI merupakan makanan lain selain ASI. Makanan ini dapat berupa makanan yang disiapkan secara khusus atau makanan yang dimodifikasi (Lilian Juwono, 2003). Sedangkan menurut Dep.Kes RI (2007), MP-ASI merupakan makanan peralihan dan dari ASI ke makanan keluarga.</p>
                                <p>Bertambahnya umur bayi, bertambah pula kebutuhan gizinya, sebab itu sejak umur 6 bulan bayi mulai diberi makanan pendamping ASI (MP-ASI). Selain ASI untuk memenuhi kebutuhan gizi perlu diperhatikan waktu pemberian, frekuensi, porsi, pemilihan bahan makanan, cara pembuatan dan cara pemberian MP-ASI.</p>
                        
                                <h2>B. Tujuan Pemberian MP-ASI</h2>
                                <ol>
                                    <li>Memenuhi kebutuhan zat gizi yang meningkat untuk pertumbuhan dan aktivitasnya.</li>
                                    <li>Mendidik anak untuk membina selera dan kebiasaan makan yang sehat.</li>
                                    <li>Melatih pencernaan bayi agar mampu mencerna makanan yang lebih padat daripada susu. Membiasakan bayi mengkonsumsi makanan sehari-hari menggunakan sendok.</li>
                                </ol>
                                
                                <h2>C. Manfaat MP-ASI</h2>
                                <p>Untuk menambah energi dan zat gizi yang diperlukan bayi karena ASI tidak dapat mencukupi kebutuhan bayi secara terus-menerus. Pertumbuhan dan perkembangan anak yang normal dapat diketahui dengan cara melihat kondisi pertambahan berat badan seorang anak tidak   mengalami   peningkatan,   menunjukkan   bahwa   kebutuhan energi bayi tidak terpenuhi.</p>
                            
                                <h2>D. Jenis MP-ASI</h2>
                                <p>MP-ASI yang baik adalah terbuat dari bahan makanan segar, seperti tempe, kacang-kacangan, telur ayam, hati ayam, ikan, sayur mayur, dan buah-buahan. Jenis MP-ASI yang dapat diberikan adalah:</p>
                                <ol>
                                    <li>Makanan Lumat adalah makanan yang dihancurkan atau disaring tampak kurang merata dan bentuknya lebih kasar dari makanan lumat halus, contoh: bubur susu, bubur sumsum, pisang saring/kerok, pepaya saring, tomat saring dan nasi tim saring.</li>
                                    <li>Makanan Lunak adalah makanan yang dimasak dengan banyak air dan tampak berair, contoh: bubur nasi, bubur ayam, nasi tim dan kentang puri.</li>
                                    <li>Makanan Padat adalah makanan lunak yang tidak nampak berair dan  biasanya  disebut makanan  keluarga,  contoh: lontong,  nasi tim, kentang rebus dan biscuit.</li>
                                </ol>
                                
                                <p>Saat mendiskusikan makanan yang baik, akan bermanfaat jika kita mulai dengan makanan pokok kemudian memutuskan makanan lain yang akan ditambahkan. Makanan Pokok adalah dimana semua masyarakat  mempunyai  makanan  pokok.  Makanan  pokok merupakan makanan utama yang dikonsumsi. Contohnya adalah serealia (misalnya beras, gandum, jagung, padi-padian, umbi- umbian).</p>
                                
                                <h2>E. Syarat-syarat MP-ASI</h2>
                                <p>Syarat-syarat MP-ASI adalah makanan pendamping ASI harus memenuhi persyaratan khusus tentang jumlah zat-zat gizi yang diperlukan bayi, seperti protein, energi, lemak, vitamin, mineral, dan zat-zat tambahan lainnya. Makanan pendamping ASI hendaknya mengandung  protein  bermutu  tinggi  dengan  jumlah  yang mencukupi. Sedangkan menurut Lilian Juwono (2004) makanan pendamping ASI yang memenuhi syarat adalah:</p>
                                <ol>
                                    <li>Kaya energi, protein, dan mikronutrien (terutama zat besi, zink, kalsium, vitamin A, vitamin C, dan folat).</li>
                                    <li>Bersih  dan  aman,  yaitu  tidak  ada  pathogen  (tidak ada  bakteri penyebab penyakit atau organisme yang berbahaya lainnya), tidak ada bahan kimia yang berbahaya atau toksin, tidak ada potongan tulang atau bagian yang keras atau yang membuat anak tersedak, tidak terlalu panas.</li>
                                    <li>Tidak terlalu pedas atau asin.</li>
                                    <li>Mudah dimakan oleh anak.</li>
                                    <li>Disukai anak.</li>
                                    <li>Tersedia di daerah anda dan harganya terjangkau.</li>
                                    <li>Mudah disiapkan.</li>
                                
                                </ol>
                                
                                <h2>F. Waktu pemberian MP-ASI</h2>
                                <p>Makanan  tambahan  diberikan  setelah  masa  ASI  eksklusif untuk memenuhi kebutuhan nutrisi dan energi, yang tidak lagi terpenuhi dari ASI saja. Di masa penyapihan ini bayi akan mendapatkan ASI, buah, biskuit bayi, bubur bayi dan lebih lanjut akan mendapat nasi tim. Prinsip pemberian makanan pada bayi usia 0 sampai 6 bulan hingga 1 tahun adalah peralihan bertahap dari hanya ASI hingga mencapai pola makan dewasa. Perubahan terjadi di dalam hal tekstur (halus hingga kasar), konsistensi (lunak hingga padat), porsi dan frekuensinya sesuai dengan kemampuan dan perkembangan bayi. Tahapan pemberian makanan pendamping ASI yang ideal adalah mulai usia 6 bulan.</p>
                                <p>Makanan tambahan harus mulai diberikan ketika bayi tidak lagi mendapat cukup energi dan nutrisi dari ASI saja. Untuk kebanyakan bayi, makanan tambahan mulai diberikan pada usia 6 bulan keatas. Pada usia ini otot dan syaraf didalam mulut bayi cukup berkembang untuk mengunyah, menggigit dan memamah. Sebelum usia 6 bulan, bayi akan mendorong makanan keluar dari mulutnya karena mereka tidak dapat mengendalikan gerakan lidahnya secara penuh. Pada usia 6 bulan lebih mudah untuk memberikan bubur kental, sup kental dan makanan yang dilumatkan, karena anak pada usia ini mempunyai kemampuan yaitu :</p>
                                <ol>
                                    <li>Dapat mengendalikan lidahnya lebih baik</li>
                                    <li>Mulai melakukan gerak mengunyah keatas dan kebawah</li>
                                    <li>Mulai tumbuh gigi</li>
                                    <li>Suka memasukkan sesuatu kedalam mulutnya</li>
                                    <li>Berminat terhadap rasa yang baru.</li>
                                </ol>
                            
                                <p>Ada beberapa tanda kesiapan yang menunjukkan seorang bayi telah mampu menerima makanan pendamping pertamanya :</p>
                                <ol>
                                    <li>Kesiapan Fisik</li>
                                        <ul>
                                            <li>Telah berkurang/hilangnya refleks menjulurkan lidah.</li>
                                            <li>Kemampuan motorik mulut tidak hanya mampu menghisap, namun juga mampu menelan makanan setengah padat.</li>
                                            <li>Dapat memindahkan makanan dalam mulut menggunakan lidah.</li>
                                            <li>Dapat mempertahankan posisi kepala secara stabil, tanpa bantuan.</li>
                                            <li>Dapat diposisikan duduk dan mampu mempertahankan keseimbangan badan.</li>
                                        </ul>
                                    <li>Kesiapan psikologis</li>
                                        <ul>
                                            <li>Perilaku yang semula hanya bersifat refleks dan imitative menjadi lebih independent dan mampu bereksplorasi.</li>
                                            <li>Menunjukkan keinginan makan dengan membuka mulut, dan menunjukkan rasa lapar dengan mencondongkan badan ketika disodori makanan.</li>
                                            <li>Sebaliknya, mampu menjauhkan badan ketika telah merasa kenyang.</li>
                                        </ul>
                                </ol>
                                
                                
                                <h2>G. Cara Pemberian MP-ASI</h2>
                                <ol>
                                    <li>Setelah bayi berusia 6 bulan perkenalkan ke makanan yang padat atau dicincang halus (Annie Yelland, 2005) seperti:</li>
                                    <ul>
                                        <li>Daging ayam yang dihaluskan.</li>
                                        <li>Kacang-kacangan yang dihaluskan.</li>
                                        <li>Yogurt: tanpa pemanis yang biasanya disukai bayi atau tambahkan buah segar cincang.</li>
                                        <li>Kembang kol dengan keju.</li>
                                        <li>Nasi.</li>
                                        <li>Ikan, buang tulang lalu cincang atau haluskan.</li>
                                    </ul>
                                    <li>Pemberian MP-ASI pada bayi usia 6 sampai 9</li>
                                    <ul>
                                        <li>Penyerapan vitamin A dan zat gizi lain pemberian ASI diteruskan.</li>
                                        <li>Pada umur 6 bulan alat cerna sudah lebih berfungsi, oleh karena itu bayi mulai diperkenalkan dengan MP-ASI lumat 2 kali sehari.</li>
                                        <li>Untuk mempertinggi nilai gizi makanan, nasi tim bayi ditambah sedikit demi sedikit dengan sumber lemak, yaitu santan atau minyak kelapa atau margarin. Bahan makanan ini dapat menambah kalori makanan bayi, memberikan rasa enak juga mempertinggi yang larut dalam lemak.</li>
                                    </ul>
                                    <li>Pemberian makanan bayi umur 9 sampai 12 bulan</li>
                                    <ul>
                                        <li>Pada umur 10 bulan bayi mulai diperkenalkan dengan makanan keluarga secara bertahap. Bentuk dan kepadatan nasi tim bayi harus diatur secara berangsur mendekati makanan keluarga.</li>
                                        <li>Berikan makanan selingan satu kali sehari. Pilihlah makanan selingan yang bernilai gizi tinggi, seperti bubur kacang hijau dan buah. Usahakan makanan selingan dibuat sendiri agar kebersihannya terjamin.</li>
                                        <li>Bayi perlu diperkenalkan dengan beraneka ragam makanan. Campurkanlah kedalam makanan lembek sebagai lauk pauk dan sayuran secara bergantian. Pengenalan berbagai bahan makanan sejak dini akan berpengaruh baik terhadap kebiasaan makan yang sehat di kemudian hari.</li>
                                    </ul>
                                    <li>Pemberian makanan bayi umur 12 sampai 24 bulan</li>
                                    <ul>
                                        <li>Pemberian ASI diteruskan.</li>
                                        <li>Pemberian MP-ASI atau makanan keluarga sekurang-kurangnya tiga kali sehari dengan porsi separuh makanan orang dewasa setiap kali makan. Selain itu tetap berikan makanan selingan dua kali sehari.</li>
                                        <li>Variasi makanan diperhatikan dengan menggunakan padanan bahan makanan, misalnya nasi diganti tahu, tempe, kacang hijau, telur atau ikan. Bayam dapat diganti dengan daun kangkung, wortel dan tomat. Bubur susu dapat diganti dengan bubur kacang hijau, bubur sumsum dan biskuit.</li>
                                        <li>Menyapih anak harus bertahap, jangan dilakukan secara tiba-tiba. Kurangi frekuensi pemberian ASI sedikit demi sedikit.</li>
                                    </ul>
                                </ol>
                                
                                <h2>H. Faktor-Faktor yang Mempengaruhi Pemberian MP-ASI terlalu dini</h2>
                                <p>Faktor-faktor yang mempengaruhi pemberian MP-ASI terlalu dini adalah:</p>
                                <ol>
                                    <li>Faktor internal meliputi: pengetahuan ibu tentang MP-ASI dan pengalaman.</li>
                                    <li>Faktor eksternal meliputi: sosial budaya, perawat atau petugas kesehatan lainnya, informasi tentang pemberian MP-ASI.</li>
                                </ol>
                            </body>
                            </html>
                        """.trimIndent()
        webView.loadDataWithBaseURL(null, htmlContent, "text/html", "utf-8", null)
    }
}