package com.material.empasianak

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MenuAwalActivity:AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.menu_awal)

        val btn_login: Button = findViewById(R.id.btn_proses_simpan)
        btn_login.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            //finish()
        }

        val btn_register: TextView = findViewById(R.id.btn_register)
        btn_register.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
            //finish()
        }
    }

    override fun onResume() {
        super.onResume()

        if (isLoggedIn()) {
            val intent = Intent(this, MenuUtamaActivity::class.java)
            startActivity(intent)
            finish() // Menyelesaikan aktivitas ini
        }
    }
    private fun isLoggedIn(): Boolean {
        val sharedPreferences = getSharedPreferences("login_pref", Context.MODE_PRIVATE)
        return sharedPreferences.getBoolean("isLoggedIn", false)
    }
}