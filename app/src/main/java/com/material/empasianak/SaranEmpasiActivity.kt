package com.material.empasianak

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient

class SaranEmpasiActivity : AppCompatActivity() {
    private lateinit var webView: WebView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.webview_layout)

        // Inisialisasi WebView
        webView = findViewById(R.id.webview_information)

        // Pengaturan WebView
        webView.settings.javaScriptEnabled = true
        webView.webChromeClient = WebChromeClient()
        webView.webViewClient = WebViewClient()

        // Muat konten HTML ke WebView
        val htmlContent = """
                            <!DOCTYPE html>
                            <html>
                            <head>
                                <title>SARAN EMPASI</title>
                                <style>
                                    p{text-align: justify;
                                        text-justify: inter-word;}
                                        h1{font-size:20px;}
                                        h2{font-size:18px;}
                                    table, th, td {
                                      border: 1px solid black;
                                      border-collapse: collapse;
                                      padding:5px;
                                    }
                                </style>
                            </head>
                            <body style="padding:10px;font-size:13px">
                                <h1 style="font-size:18px">SARAN EMPASI</h1>
                        
                                <h2>A. Jenis MP-ASI</h2>
                                <table>
                                    <tr>
                                        <th>Usia</th>
                                        <th>Tekstur</th>
                                        <th>Bentuk Makanan</th>
                                    </tr>
                                    <tr>
                                        <td>6-7 bulan</td>
                                        <td>Halus, encer dan lembut</td>
                                        <td>Pure, bubur beras</td>
                                    </tr>
                                    <tr>
                                        <td>8-10 bulan</td>
                                        <td>Lembek dan lembut</td>
                                        <td>Bubur saring</td>
                                    </tr>
                                    <tr>
                                        <td>11-12 bulan</td>
                                        <td>Semi padat</td>
                                        <td>Nasi tim, makanan yang dicincang kasar</td>
                                    </tr>
                                    <tr>
                                        <td>>12 bulan</td>
                                        <td>Makanan padat</td>
                                        <td>Nasi, kentang</td>
                                    </tr>
                                </table>
                                
                                <h2>B. Jadwal Makan Sehat</h2>
                                
                                <h3>1. Usia 6-7 bulan</h3>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Waktu</th>
                                            <th>Menu</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Pagi</td>
                                            <td>ASI/Susu formula</td>
                                        </tr>
                                        <tr>
                                            <td>Sarapan</td>
                                            <td>Bubur susu beras merah</td>
                                        </tr>
                                        <tr>
                                            <td>Selingan pagi</td>
                                            <td>Pure pisang</td>
                                        </tr>
                                        <tr>
                                            <td>Makan siang</td>
                                            <td>Bubur susu labu kuning</td>
                                        </tr>
                                        <tr>
                                            <td>Selingan sore</td>
                                            <td>ASI/Susu formula</td>
                                        </tr>
                                        <tr>
                                            <td>Makan malam</td>
                                            <td>Pure kentang susu</td>
                                        </tr>
                                        <tr>
                                            <td>Menjelang tidur</td>
                                            <td>ASI/Susu formula</td>
                                        </tr>
                                    </tbody>
                                </table>
                                
                                <h3>2. Usia 8-10 bulan</h3>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Waktu</th>
                                            <th>Menu</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Pagi</td>
                                            <td>ASI/Susu formula</td>
                                        </tr>
                                        <tr>
                                            <td>Sarapan</td>
                                            <td>Nasi lunak hati ayam</td>
                                        </tr>
                                        <tr>
                                            <td>Selingan pagi</td>
                                            <td>Pure buah campur</td>
                                        </tr>
                                        <tr>
                                            <td>Makan siang</td>
                                            <td>Bubur kentang tempe</td>
                                        </tr>
                                        <tr>
                                            <td>Selingan sore</td>
                                            <td>Potongan mangga</td>
                                        </tr>
                                        <tr>
                                            <td>Makan malam</td>
                                            <td>Bubur nasi ikan</td>
                                        </tr>
                                        <tr>
                                            <td>Menjelang tidur</td>
                                            <td>ASI/Susu formula</td>
                                        </tr>
                                    </tbody>
                                </table>
                                
                                <h3>3. Usia 11-12 bulan</h3>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Waktu</th>
                                            <th>Menu</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Pagi</td>
                                            <td>ASI/Susu formula</td>
                                        </tr>
                                        <tr>
                                            <td>Sarapan</td>
                                            <td>Nasi tim ayam sayuran</td>
                                        </tr>
                                        <tr>
                                            <td>Selingan pagi</td>
                                            <td>Potongan buah pepaya</td>
                                        </tr>
                                        <tr>
                                            <td>Makan siang</td>
                                            <td>ASI/Susu formula</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>Tim daging beras merah</td>
                                        </tr>
                                        <tr>
                                            <td>Selingan sore</td>
                                            <td>Biskuit</td>
                                        </tr>
                                        <tr>
                                            <td>Makan malam</td>
                                            <td>Nasi tim ikan</td>
                                        </tr>
                                        <tr>
                                            <td>Menjelang tidur</td>
                                            <td>ASI/Susu formula</td>
                                        </tr>
                                    </tbody>
                                </table>
                                
                                <h3>4. Usia 13-24 bulan</h3>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Waktu</th>
                                            <th>Menu</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Pagi</td>
                                            <td>ASI/Susu formula</td>
                                        </tr>
                                        <tr>
                                            <td>Sarapan</td>
                                            <td>Nasi bayam</td>
                                        </tr>
                                        <tr>
                                            <td>Selingan pagi</td>
                                            <td>Puding</td>
                                        </tr>
                                        <tr>
                                            <td>Makan siang</td>
                                            <td>ASI/Susu formula</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>Nasi sop ikan</td>
                                        </tr>
                                        <tr>
                                            <td>Selingan sore</td>
                                            <td>Biskuit</td>
                                        </tr>
                                        <tr>
                                            <td>Makan malam</td>
                                            <td>Nasi ikan goreng</td>
                                        </tr>
                                        <tr>
                                            <td>Menjelang tidur</td>
                                            <td>ASI/Susu formula</td>
                                        </tr>
                                    </tbody>
                                </table>
                                
                                <h2>C. Resep Makanan</h2>
                                <h3>1. Usia 6-7 Bulan</h3>
                                    <h4>a. Pure Kentang Susu</h4>
                                    <img src="file:///android_res/drawable/purekentangsusu.jpg" alt="Pure Kentang Susu" width="180px" />
                                    <br><b>Bahan :</b><br>
                                    <table>
                                        <tr><td width="20px">1.</td><td>100 gram kentang kupas potong dadu</td></tr>
                                        <tr><td width="20px">2.</td><td>100 ml kaldu daging atau air matang</td></tr>
                                        <tr><td width="20px">3.</td><td>100 ml susu/ASI</td></tr>
                                    </table>
                                    
                                    <br><b>Cara Membuat :</b><br>
                                    <table>
                                        <tr><td width="20px">1.</td><td>Kukus kentang hingga matang dan teksturnya lunak</td></tr>
                                        <tr><td width="20px">2.</td><td>Masukkan kentang dan kaldu daging/ air ke dalam blender. Haluskan hingga lembut.</td></tr>
                                        <tr><td width="20px">3.</td><td>Tambahkan susu/ASI</td></tr>
                                        <tr><td width="20px">4.</td><td>Tuangkan puree ke wadah makanan. Lalu sajikan menggunakan mangkuk bayi aman.</td></tr>
                                    </table>
                                    
                                    <h4>b. Pure Pisang</h4>
                                    <img src="file:///android_res/drawable/purepisang.jpg" alt="Pure Kentang Susu" width="180px" />
                                    <br><b>Bahan :</b><br>
                                    <table>
                                        <tr><td width="20px">1.</td><td>200 ml Air</td></tr>
                                        <tr><td width="20px">2.</td><td>20 gram Biskuit Bayi</td></tr>
                                        <tr><td width="20px">3.</td><td>50 gram Pisang</td></tr>
                                        <tr><td width="20px">4.</td><td>3 sendok makan Susu Formula Bubuk </td></tr>
                                    </table>
                                    
                                    <br><b>Cara Membuat :</b><br>
                                    <table>
                                        <tr><td width="20px">1.</td><td>Pertama haluskan biskuit bayi dan pisang.</td></tr>
                                        <tr><td width="20px">2.</td><td>Kemudian masak air hingga mendidih</td></tr>
                                        <tr><td width="20px">3.</td><td>Selanjutnya masukkan biskuit aduk rata kemudian angkat tambahkan susu formula</td></tr>
                                        <tr><td width="20px">4.</td><td>Kemudian kemudian tambahkan pisang yg telah dihaluskan aduk rata kembali</td></tr>
                                        <tr><td width="20px">5.</td><td>Lalu setelah agak dingin siap disajikan.</td></tr>
                                    </table>
                                    
                                    <h4>c. Sari Jeruk</h4>
                                    <img src="file:///android_res/drawable/sarijeruk.jpg" alt="Sari Jeruk" width="180px" />
                                    <br><b>Bahan :</b><br>
                                    <table>
                                        <tr><td width="20px">1.</td><td>1 buah jeruk manis (jeruk baby atau jeruk pontianak)</td></tr>
                                    </table>
                                    
                                    <br><b>Cara Membuat :</b><br>
                                    <table>
                                        <tr><td width="20px">1.</td><td>Cuci buah jeruk hingga benar-benar bersih</td></tr>
                                        <tr><td width="20px">2.</td><td>Belah jeruk menjadi dua bagian dan iris melintang</td></tr>
                                        <tr><td width="20px">3.</td><td>eras buah jeruk menggunakan perasan khusus</td></tr>
                                        <tr><td width="20px">4.</td><td>Tuangkan air perasan ke dalam cangkir kecil dan sajikan pada bayi menggunakan sendok</td></tr>
                                    </table>
                                    
                                    
                                    <h4>d. Bubur Susu Labu Kuning</h4>
                                    <img src="file:///android_res/drawable/susulabukuning.jpg" alt="Susu Labu Kuning" width="180px" />
                                    <br><b>Bahan :</b><br>
                                    <table>
                                        <tr><td width="20px">1.</td><td>100 gram labu kuning , kukus haluskan</td></tr>
                                        <tr><td width="20px">2.</td><td>1/2 sdm tepung beras</td></tr>
                                        <tr><td width="20px">3.</td><td>2 sdm susu formula lanjutan sesuai usia, larutkan dengan 60 ml air matang</td></tr>
                                    </table>
                                    
                                    <br><b>Cara Membuat :</b><br>
                                    <table>
                                        <tr><td width="20px">1.</td><td>Larutkan tepung beras dengan air, aduk rata. Masak dengan air mendidih dan kental</td></tr>
                                        <tr><td width="20px">2.</td><td>Masukkan labu kunng, didihkan kembali. Angkat, kemudian tuang larutan susu formula, aduk rata</td></tr>
                                        <tr><td width="20px">3.</td><td>Tuang kedalam mangkok saji, dan bubur susu labu kuning siap disajikan</td></tr>
                                    </table>
                                    
                                    <h4>e. Bubur Susu Beras Merah</h4>
                                    <img src="file:///android_res/drawable/susuberasmerah.jpg" alt="Susu Beras Merah" width="180px" />
                                    <br><b>Bahan :</b><br>
                                    1. 50 gram beras merah, rendam, tiriskan, haluskan, dan ayak<br>
                                    2. 300 kaldu kaki ayam<br>
                                    3. 2 takar susu formula lanjutan / 100 ml ASI<br>
                                    4. 25 g pisang ambon, keruk dan haluskan <br>
                                    
                                    <br><b>Cara Membuat :</b><br>
                                    1. Campur tepung beras merah dengan kaldu ayam, aduk rata. Jerang di atas api sambil diaduk hingga matang. Angkat.<br>
                                    2. Tambahkan susu formula / ASI dan pisang, aduk rata.<br>
                                    3. Berikan pada bayi selagi hangat <br>
                                    
                                    <h4>e. Bubur Susu Daun Bayam</h4>
                                    <img src="file:///android_res/drawable/susudaunbayam.jpg" alt="Susu Daun Bayam" width="180px" />
                                    <br><b>Bahan :</b><br>
                                    1. 20 gr bayam, bersihkan<br>
                                    2. 50 ml susu formula atau ASI<br>
                                    3. 30 gr tepung beras<br>
                                    4. 100 air<br>

                                    
                                    <br><b>Cara Membuat :</b><br>
                                    1. Kukus bayam yang sudah di bersihkan tadi. Kemudian kukus higga matang. Lalu blender dan ambil sarinya. Sisihkan<br>
                                    2. Masak tepung beras dengan air hingga mengental dengan api kecil. Aduk rata. Angkat. Sisihkan<br>
                                    3. Campur sari bayam dengan bubur tepung. Kemudian tambahkan suus formula atau ASI. Aduk rata. Hidangkan hangat<br>

                                <h3>2. Usia 8-10 Bulan</h3>
                                    <h4>a. Nasi tim saring ikan salmon</h4>
                                    <img src="file:///android_res/drawable/ikansalmon.jpg" alt="Image" width="180px" />
                                    <br><b>Bahan :</b><br>
                                    1.	25 gram beras putih<br>
                                    2.	500 cc air putih<br>
                                    3.	30 gram daging ikan salmon tanpa tulang (fillet)<br>
                                    4.	50 gram tomat potong kecil-kecil<br>
                                    5.	1 sendok teh mentega<br>

                                    
                                    <br><b>Cara Membuat :</b><br>
                                    1.	Siapkan panci kemudian beras bersama dengan daging ikan salmon hingga menjadi bubur<br>
                                    2.	Tambahkan tomat kemudian aduk-aduk.<br>
                                    3.	Tambahkan mentega lalu aduk hingga bubur mengental. Sisihkan.<br>
                                    4.	Haluskan dengan blender lalu saring dengan saringan teh atau saringan kawat.<br>

                                    <h4>b.	Nasi tim saring hati ayam</h4>
                                    <img src="file:///android_res/drawable/hatiayam.jpg" alt="Image" width="180px" />
                                    <br><b>Bahan :</b><br>
                                    1.	30 gram Bayam Merah<br>
                                    2.	2 sendok makan Beras Merah<br>
                                    3.	1 buah Hati Ayam<br>
                                    4.	750 ml Kaldu Ayam<br>
                                    5.	1 buah Tahu Putih<br>


                                    
                                    <br><b>Cara Membuat :</b><br>
                                    1.	Pertama cuci bersih beras merah<br>
                                    2.	Kemudian potong dadu hati ayam<br>
                                    3.	Selanjutnya potong dadu tahu putih<br>
                                    4.	Kemudian cuci bersih bayam merah dan iris halus<br>
                                    5.	Lalu campur beras merah, kaldu kaki ayam, hati ayam, dan tahu. rebus sambil diaduk-aduk hingga menjadi bubur<br>
                                    6.	Masukkan bayam, didihkan<br>
                                    7.	Masukkan asi atau susu formula yang telah dicairkan, didihkan sebentar, angkat. dinginkan.<br>
                                    8.	Saring bubur dengan saringan kawat kemudian hidangkan<br>
                                    
                                    
                                    <h4>c. Pure buah advokat</h4>
                                    <img src="file:///android_res/drawable/advokat.jpg" alt="Image" width="180px" />
                                    <br><b>Bahan :</b><br>
                                    1.	50	gram	alpukat	matang	yang masih	segar	(ambil dangingnya kemudian pitong menjadi beberapa bagian)<br>
                                    2.	25 cc ASI perah atau bisa menggunakan susu formula<br>
                                    
                                    <br><b>Cara Membuat :</b><br>
                                    1.	Siapkan blender. Kemudian masukkan buah alpukat yang telah dipotong-potong menjadi beberapa bagian kemudian tambahkan dengan ASI atau susu formula.<br>
                                    2.	Blender sampai buah alpukat benar-benar halus kira-kira 3- 4 menit.<br>
                                    3.	Sajikan dalam mangkuk kecil atau tempat makan bayi.<br>
                                    4.	Sajikan untuk satu kali makan (lebih cocok diberikan saat makan siang)<br>

                                    <h4>d. Bubur tempe</h4>
                                    <img src="file:///android_res/drawable/buburtempe.jpg" alt="Image" width="180px" />
                                    <br><b>Bahan :</b><br>
                                    1.	100 gr tempe<br>
                                    2.	Air matang/ASI/susu formula<br>
                                    
                                    <br><b>Cara Membuat :</b><br>
                                    1.	Tempe dibersihkan dan dicincang<br>
                                    2.	Kukus sampai matang<br>
                                    3.	Haluskan dengan saringan makanan bayi, atau blender dengan air secukupnya (bisa menggunakan kaldu atau ASI/susu formula)<br>
                            
                            
                            
                                    <h4>e. Sup Krim Ayam</h4>
                                    <img src="file:///android_res/drawable/supkrimayam.jpg" alt="Image" width="180px" />
                                    <br><b>Bahan :</b><br>
                                    1.	1 dada ayam tanpa tulang dan kulit<br>
                                    2.	1 cangkir ASI/susu formula<br>
                                    3.	1/2 sdt tepung maizena<br>
                                    
                                    <br><b>Cara Membuat :</b><br>
                                    1.	Hilangkan kulit, lemak dan sisa-sisa tulang pada ayam. Cuci sampai bersih, potong dadu.<br>
                                    2.	Rebus ayam dengan air secukupnya selama 10 menit atau sampai ayam matang. Angkat dan tiriskan. Simpan kaldu.<br>
                                    3.	Masukkan ayam, tepung dan kaldu ke dalam blender, haluskan.	Gunakan	kaldu	untuk mengencerkan/mengentalkan adonan hingga kekentalan yang diinginkan.<br>
                                    4.	Tim adonan sampai matang. Saat ingin menyajikan tambahkan ASI/susu formula.<br>
                            
                                    <h4>f. Bubur jagung manis</h4>
                                    <img src="file:///android_res/drawable/jagungmanis.jpg" alt="Image" width="180px" />
                                    <br><b>Bahan :</b><br>
                                    1.	1 buah jagung manis segar ( iris lalu dihaluskan )<br>
                                    2.	susu cair 175 ml<br>
                                    3.	gula pasir 3 sendok makan<br>
                                    4.	keju cheddar parut secukupnya<br>
                                    5.	tepung meizena 10 gr ( larutkan dengan sedikit air )<br>
                                    6.	1/2 sendok teh vanili bubuk<br>
                                    7.	1/2 sendok teh garam halus<br>
                                    
                                    <br><b>Cara Membuat :</b><br>
                                    1.	Rebus susu cair bersama gula pasir dan vanili bubuk diatas api sedang<br>
                                    2.	Aduk-aduk hingga mendidih dan gula larut dengan susu<br>
                                    3.	Masukkan jagung manis yang sudah dihaluskan kedalam rebusan susu<br>
                                    4.	Aduk sampai jagung menjadi setengah matang<br>
                                    5.	Tambahkan garam kedalam rebusan jagung yang sedang dimasak<br>
                                    6.	Tunggu beberapa menit	sampai jagung matang dan mengental<br>
                                    7.	Tuang larutan tepung meizena lalu aduk lagi sampai meletup-letup<br>
                                    8.	Angkat lalu diamkan sampai hangat kemudian taburi dengan keju parut dan sajikan<br>
                                    
                                    
                                <h3>3. Usia 8-10 Bulan</h3>
                                    <h4>a.	Nasi tim ayam jagung manis dan bayam</h4>
                                    <img src="file:///android_res/drawable/ayamjagungmanis.jpg" alt="Image" width="180px" />
                                    <br><b>Bahan :</b><br>
                                    1)	50 gram jagung manis<br>
                                    2)	Satu genggam bayam (iris halus)<br>
                                    3)	30 gram beras<br>
                                    4)	50 gram ayam (haluskan dengan blender)<br>
                                    5)	1 butir bawang merah (iris halus)<br>
                                    6)	1 buah wortel (potong dadu)<br>
                                    7)	150 cc kaldu ayam<br>
                                    8)	Air<br>

                                    
                                    <br><b>Cara Membuat :</b><br>
                                    1)	Rebus beras hingga matang dan menjadi bubur, tambahkan ayam yang sudah di blender. Kemudian aduk sampai rata dan masukkan kaldu ayam.<br>
                                    2)	Masukkan potongan jagung, lalu aduk secara perlahan hingga jagung matang<br>
                                    3)	Masukkan irisan bawang sambil diaduk sampai matang<br>
                                    4)	Angkat dan taburi nasi tim dengan bayam dan wortel yang sudah direbus<br>
                                    5)	Siap untuk disajikan pada bayi (sajikan saat hangat)<br>


                                    <h4>b.	Nasi tim daging beras merah</h4>
                                    <img src="file:///android_res/drawable/dagingberasmerah.jpg" alt="Image" width="180px" />
                                    <br><b>Bahan :</b><br>
                                    1)	100 gram brokoli<br>
                                    2)	50 gram daging Sapi<br>
                                    3)	1 butir kuning telur<br>
                                    4)	1 sendok teh mentega<br>
                                    5)	400 ml santan<br>
                                    6)	50 gram tomat<br>
                                    7)	100 gram beras merah<br>

                                    
                                    <br><b>Cara Membuat :</b><br>
                                    1)	Pertama rebus ubi hingga lunak, kupas dan haluskan.<br>
                                    2)	Kemudian cincang halus daging sapi.<br>
                                    3)	Selanjutnya cuci bersih brokoli dan cincang halus<br>
                                    4)	Kemudian cincang halus tomat dan buang bijinya<br>
                                    5)	Lalu campurkan semua bahan menjadi satu<br>
                                    6)	Masukkan kedalam mangkuk tahan panas<br>
                                    7)	Lalu dimasukkan kedalam mangkuk tahan panas yang lebih besar yang diberi air kemudian di oven.<br>
                                    8)	Atau kukus hingga matang lalu angkat Sajikan selagi hangat.<br>


                                    <h4>c. Tim makaroni tabur telur kuning</h4>
                                    <img src="file:///android_res/drawable/makaronitabur.jpg" alt="Image" width="180px" />
                                    <br><b>Bahan :</b><br>
                                    1)	25 gram Buncis<br>
                                    2)	1 butir Kuning Telur<br>
                                    3)	30 gram Makaroni<br>
                                    4)	1 sendok teh Minyak<br>
                                    5)	25 gram Tempe<br>
                                    6)	25 gram Wortel<br>

                                    <br><b>Cara Membuat :</b><br>
                                    1)	Pertama rebus makaroni sampai matang.<br>
                                    2)	Kemudian potong dadu tempe.<br>
                                    3)	Selanjutnya buang serat buncis dan iris halus.<br>
                                    4)	Kemudian kupas wortel, cuci bersih dan parut.<br>
                                    5)	Lalu rebus telur dan gunakan kuning telurnya saja untuk dihaluskan.<br>
                                    6)	Masukkan parutan wortel, buncis dan makaroni pada pinggan tahan panas, beri air sampai semua bahan terendam, lalu tim.<br>
                                    7)	Masukkan minyak kacang kedelai, angkat.<br>
                                    8)	Taburkan kuning telur diatasnya. Hidangkan<br>


                                    <h4>d.	Tim roti daging</h4>
                                    <img src="file:///android_res/drawable/rotidaging.jpg" alt="Image" width="180px" />
                                    <br><b>Bahan :</b><br>
                                    1)	250 ml kaldu<br>
                                    2)	25 gram daging sapi giling<br>
                                    3)	25 gram wortel<br>
                                    4)	2 sdm jagung manis pipil<br>
                                    5)	1 lembar roti tawar whole bread, potong kotak 1 cm<br>


                                    <br><b>Cara Membuat :</b><br>
                                    1)	Rebus kaldu dan daging giling sampai mendidih. Masukkan wortel, jagung manis, dan roti tawar, aduk hingga kaldu terserap.<br>
                                    2)	Angkat dari api, dinginkan.<br>
                                    3)	Blender kasar (sesuaikan dengan kebiasaan bayi), tuang ke dalam mangkuk tahan panas lalu kukus sekitar 20-25 menit.<br>

                                    
                                    <h4>e.	Tim kentang keju</h4>
                                    <img src="file:///android_res/drawable/kentangkeju.jpg" alt="Image" width="180px" />
                                    <br><b>Bahan :</b><br>
                                    1)	200 gram kentang, rebus, haluskan<br>
                                    2)	1 sdm krim keju<br>
                                    3)	50 gram daging sapi giling<br>
                                    4)	50 gram tahu, haluskan<br>
                                    5)	50 gram brokoli, cincang halus<br>
                                    6)	1 butir telur<br>
                                    7)	400 ml kaldu sapi<br>

    
                                    <br><b>Cara Membuat :</b><br>
                                    1)	Campur semua bahan lalu aduk rata<br>
                                    2)	Tematkan adonan pada mangkuk tahan panas lalu tutup dengan aluminium foil.<br>
                                    3)	Panggang dalam oven sampai matang<br>
                                    4)	Sajikan setelah agak dingin<br>


                                <h3>3. Usia 13-24 bulan</h3>
                                    <h4>a.	Nasi bayam</h4>
                                    <img src="file:///android_res/drawable/nasibayam.jpg" alt="Image" width="180px" />
                                    <br><b>Bahan :</b><br>
                                    1)	350 gram beras, cuci bersih<br>
                                    2)	2 siung bawang putih, memarkan<br>
                                    3)	1 cm jahe, memarkan<br>
                                    4)	150 gram paha ayam fillet, potong dadu<br>
                                    5)	100 gram jamur kancing, belah 2 bagian<br>
                                    6)	1/2 sendok makan garam<br>
                                    7)	1/4 sendok teh merica bubuk<br>
                                    8)	200 ml air bayam, dari 100 gram bayam dan 25 ml air, blender, saring<br>
                                    9)	250 ml kaldu ayam, dari rebusan tulang ayam<br>
                                    10)	1 sendok makan minyak untuk menumis<br>

                                    <br><b>Cara Membuat :</b><br>
                                    1)	Tumis bawang putih dan jahe sampai harum. Tambahkan ayam. Aduk sampai berubah warna. Masukkan jamur kancing. Aduk rata.<br>
                                    2)	Masukkan beras, garam, dan merica bubuk. Aduk rata.<br>
                                    3)	Tuang air bayam dan kaldu ayam sambil diaduk rata. Masak sampai meresap.<br>
                                    4)	Kukus 45 menit di atas api sedang sampai matang.<br>
                                    5)	Untuk 4 porsi<br>


                                
                                    <h4>b.	Nasi sup ikan</h4>
                                    <img src="file:///android_res/drawable/nasisupikan.jpg" alt="Image" width="180px" />
                                    <br><b>Bahan :</b><br>
                                    1)	2 ekor ikan kakap, potong masing-masing jadi 3 bagian<br>
                                    2)	10 buah belimbing wuluh<br>
                                    3)	50 gr daun kemangi<br>
                                    4)	800 ml air<br>

                                    <br><b>Bumbu Halus</b><br>
                                    1)	3 butir bawang putih<br>
                                    2)	3 buah cabai merah<br>
                                    3)	2 cm jahe<br>
                                    4)	1 cm kunyit<br>
                                    5)	1 sdm garam<br>
                                    6)	1 batang serai, memarkan<br>
                                    7)	2 lembar daun jeruk<br>
                                    8)	1 buah tomat, iris dadu<br>

                                    <br><b>Cara Membuat :</b><br>
                                    1)	Didihkan air, masukkan bumbu halus, serai, daun jeruk dan ikan. Masak hingga bumbu meresap ke dalam ikan.<br>
                                    2)	Masukkan tomat, belimbing wuluh, dan daun kemangi. Masak hingga daun kemangi layu.<br>
                                    3)	Angkat, sajikan hangat.<br>
                                    
                                    <h4>c.	Nasi ikan goreng</h4>
                                    <img src="file:///android_res/drawable/ikangoreng.jpg" alt="Image" width="180px" />
                                    <br><b>Bahan :</b><br>
                                    1)	4 ekor ikan nila ukuran sedang, dikerat-kerat<br>
                                    2)	2 sendok teh air jeruk nipis<br>
                                    3)	minyak untuk menggoreng<br>

                                    <br><b>Bumbu Halus</b><br>
                                    1)	3 siung bawang putih, dihaluskan<br>
                                    2)	2 buah kemiri, disangrai, dihaluskan<br>
                                    3)	1 sendok teh ketumbar bubuk<br>
                                    4)	150 ml air<br>
                                    5)	2 sendok teh garam<br>
                                    6)	1/4 sendok teh merica bubuk<br>

                                    <br><b>Bahan Pelapis (aduk Rata):</b><br>
                                    1)	200 gram tepung protein sedang<br>
                                    2)	50 gram tepung beras<br>
                                    3)	1/2 sendok teh baking powder<br>
                                    4)	1/2 sendok teh garam<br>
                                    5)	1/4 sendok teh merica bubuk<br>

                                    <br><b>Cara mengolah ikan nila goreng crispy:</b><br>
                                    1)	Lumuri ikan dengan air jeruk nipis dan bumbu halus. Diamkan 15 menit.<br>
                                    2)	Gulingkan di atas bahan pelapis. Kibas-kibaskan.<br>
                                    3)	Goreng dalam minyak yang sudah dipanaskan di atas api sedang hingga matang.<br>
                                    4)	Untuk 4 porsi<br>

                                    
                            </body>
                            </html>
                        """.trimIndent()
        webView.loadDataWithBaseURL(null, htmlContent, "text/html", "utf-8", null)
    }
}