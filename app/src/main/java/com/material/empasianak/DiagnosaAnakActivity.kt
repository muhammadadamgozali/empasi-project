package com.material.empasianak

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.database.Cursor
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import com.google.android.material.slider.Slider
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class DiagnosaAnakActivity : AppCompatActivity() {
    private lateinit var dbHelper: DatabaseHelper
    var panjang_badan_min : Float = 0.0f
    var panjang_badan_max : Float = 0.0f
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.diagnosa_anak_layout)

        dbHelper = DatabaseHelper(this)

        val autoCompleteJenisKelamin : AutoCompleteTextView = findViewById(R.id.autoCompleteJenisKelamin)
        val sliderUsiaAnak : Slider = findViewById(R.id.slider_usia_anak)
        val hasilUsiaAnak : TextView = findViewById(R.id.tv_hasil_usia_anak)

        val layoutPanjangBadan : TextInputLayout = findViewById(R.id.textlayout_panjang_badan)
        val inputPanjangBadan : TextInputEditText = findViewById(R.id.it_panjang_badan)

        val layoutBeratBadan : TextInputLayout = findViewById(R.id.textlayout_berat_badan)
        val beratBadan : TextInputEditText = findViewById(R.id.it_berat_badan)

        val btnProsesHitung : Button = findViewById(R.id.btn_proses_simpan)


        autoCompleteJenisKelamin.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // Method ini dipanggil sebelum teks berubah
                sliderUsiaAnak.isEnabled = false
                layoutPanjangBadan.isEnabled = false
                sliderUsiaAnak.value = 0F

                hasilUsiaAnak.text = ""

                btnProsesHitung.visibility  = View.INVISIBLE
                btnProsesHitung.isEnabled = false
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // Method ini dipanggil ketika teks berubah
                val newText = s.toString()
                // Lakukan sesuatu dengan teks yang baru
            }

            override fun afterTextChanged(s: Editable?) {
                // Method ini dipanggil setelah teks berubah
                // Lakukan sesuatu dengan teks yang baru, misalnya menampilkan Toast
                sliderUsiaAnak.isEnabled = true
            }
        })

        sliderUsiaAnak.addOnSliderTouchListener(object : Slider.OnSliderTouchListener {
            override fun onStartTrackingTouch(slider: Slider) {
                // Method ini dipanggil saat pengguna mulai menyentuh slider
            }

            override fun onStopTrackingTouch(slider: Slider) {
                // Method ini dipanggil saat pengguna mengangkat jari dari slider
                val UsiaAnak_Text = slider.value.toInt();
                val JenisKelamin_Text = autoCompleteJenisKelamin.text.toString();

                if(UsiaAnak_Text >=0 && UsiaAnak_Text <=24){
                    layoutPanjangBadan.hint = "Panjang Badan (45 - 110)"
                    panjang_badan_min = 45F
                    panjang_badan_max = 110F
                }else if(UsiaAnak_Text >24 && UsiaAnak_Text <= 60){
                    layoutPanjangBadan.hint = "Panjang Badan (65 - 120)"
                    panjang_badan_min = 65F
                    panjang_badan_max = 120F
                }

                hasilUsiaAnak.text = UsiaAnak_Text.toString()
                layoutPanjangBadan.isEnabled = true
                layoutBeratBadan.isEnabled = true

                btnProsesHitung.visibility  = View.VISIBLE
                btnProsesHitung.isEnabled = true

            }
        })


        btnProsesHitung.setOnClickListener {

            // Buat intent untuk pindah ke halaman hasil
            val intent = Intent(this, HasilDiagnosaActivity::class.java)
            val JenisKelamin_Text = autoCompleteJenisKelamin.text.toString();
            val UsiaAnak_Text = sliderUsiaAnak.value.toString();
            val PanjangBadan_Text = inputPanjangBadan.text.toString();
            val BeratBadan_Text = beratBadan.text.toString();

            if(PanjangBadan_Text.toFloat() >= panjang_badan_min && PanjangBadan_Text.toFloat() <= panjang_badan_max){
                // Masukkan data yang akan dikirim ke halaman hasil
                intent.putExtra("usia_anak", UsiaAnak_Text)
                intent.putExtra("jenis_kelamin", JenisKelamin_Text)
                intent.putExtra("panjang_badan", PanjangBadan_Text)
                intent.putExtra("berat_badan", BeratBadan_Text)

                // Pindah ke halaman hasil
                startActivity(intent)
            }else{
                Toast.makeText(this, "Panjang Badan Diluar Kriteria", Toast.LENGTH_SHORT).show()
            }


        }
    }

    override fun onResume() {
        super.onResume()

        if (!isLoggedIn()) {
            val intent = Intent(this, MenuUtamaActivity::class.java)
            startActivity(intent)
            finish() // Menyelesaikan aktivitas ini
        }
    }
    private fun isLoggedIn(): Boolean {
        val sharedPreferences = getSharedPreferences("login_pref", Context.MODE_PRIVATE)
        return sharedPreferences.getBoolean("isLoggedIn", false)
    }



}